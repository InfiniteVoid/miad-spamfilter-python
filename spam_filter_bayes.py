import sys
import os
import re
from operator import mul
from functools import reduce

# Регулярний вираз для пошуку слів
word_regex = re.compile("[a-zA-Z']+(?:-[a-zA-Z']+)?")

# Отримати текст всіх повідомлень з папки
def get_messages(folder):
    messages = {}
    encoding = sys.stdout.encoding
    filenames = list(os.walk(folder))[0][2]
    
    # Зчитати з файла
    for filename in filenames:
        path = folder + "/" + filename
        
        with open(path, encoding=encoding, errors="ignore") as message_file:
            # Додати повідомлення в словник повідомлень
            messages[filename] = message_file.read()
    
    return messages

# Отримати всі слова в повідомленні
def get_messages_words(messages):
    words_in_messages = {}
    
    for key, message in messages.items():
        words = word_regex.findall(message)
        num_words = len(words)
        message_words = {}
        
        # Кількість повторень слова
        for i in range(num_words):
            if words[i] not in message_words:
                message_words[words[i]] = ""
        
        words_in_messages[key] = message_words
    
    return words_in_messages

# Отримати частоту появлення кожного слова у переліку повідомлень
def get_word_frequencies(words):
    word_frequencies = {}
    total_messages = len(words)
    
    # Вираховуємо кількісь повторень одного слова в повідомленні
    for key, message in words.items():
        for word in message:
            if word not in word_frequencies:
                word_frequencies[word] = 1
            else:
                word_frequencies[word] += 1
    
    # Вираховуємо відсоток появи слова у повідомлення
    for word, occurences in word_frequencies.items():
        word_frequencies[word] = (occurences / total_messages, occurences)
    
    return word_frequencies

# Отримати оцінку слова на спам
def get_word_spamicities(spam_word_frequencies, ham_word_frequencies,
                         init_prob_spam, occurence_threshold):
    spamicities = {}
    words = set(list(spam_word_frequencies) + list(ham_word_frequencies))
    
    for word in words:
        spam_word_occurences = spam_word_frequencies[word][1] if word in spam_word_frequencies else 0
        ham_word_occurences = ham_word_frequencies[word][1] if word in ham_word_frequencies else 0
        
        # Виключити слова які зузстрічаються меньше ніж задана мінімальна кількість в змінній occurence_threshold
        if spam_word_occurences + ham_word_occurences >= occurence_threshold:
            # Якщо слово представлене в спамі і в не спамі 
            if word in spam_word_frequencies and word in ham_word_frequencies:
                
                # Формула розрахунку чи є повідомлення, що містить данне слово, спамом
                #                 P(W|S) * P(S)
                # P(S|W) = -----------------------------
                #          P(W|S) * P(S) + P(W|H) * P(H)
                # P(S|W) - вірогідність того що повідомлення, яке містить данне слово є спамом
                # P(W|S) - вірогідність того що спам повідомлення, містить данне слово
                # P(W|H) - вірогідність того що звичайне повідомлення, містить данне слово
                # P(S)   - початкова вірогідність що дане повідомлення це спам (опріорна вірогідність)
                # P(H)   - початкова вірогідність що дане повідомлення не є спамам (опріорна вірогідність)

                prob_word_spam = spam_word_frequencies[word][0] * init_prob_spam
                prob_word_ham = ham_word_frequencies[word][0] * (1 - init_prob_spam)
                spamicities[word] = prob_word_spam / (prob_word_spam + prob_word_ham)

            # Слово відсутне в списку слів для спам повідомлень
            elif spam_word_occurences == 0:
                spamicities[word] = 0.01
            # Слово відсутне в списку слів для не спам повідомлень
            elif ham_word_occurences == 0:
                spamicities[word] = 0.99
    
    return spamicities

# Отримати оцінку чи повідомлення являється спамом
def get_spam_score(message, word_spamicities):
    # Ігнорувати слова які не повторювались достатню кількість разів. Дивись функцію get_word_spamicities
    message = [word for word in message if word in word_spamicities]
    
    # Сформувати масив з оцінкою спамності для кожного слова
    spamicities = [(word, word_spamicities[word]) for word in message]
    # Сортуэмо і отримуємо слова з найбільшим відхиленняв від норми (0.5) в сторону спаму
    top_spamicities = sorted(spamicities, key=lambda x: abs(0.5 - x[1]), reverse=True)[:10]
    
    # Вірогідність того, що дане повідомлення це спам
    #                     p1 p2 ... pN
    # P = --------------------------------------------
    #     p1 p2 ... pN + (1 - p1)(1 - p2) ... (1 - pN)
    # P - вірогідність того, що дане повідомлення це спам
    # pN - спамність кожного слова в повідомленні
    
    prob_spam = reduce(mul, [x[1] for x in top_spamicities])
    prob_spam_inv = reduce(mul, [1 - x[1] for x in top_spamicities])
        
    return prob_spam / (prob_spam + prob_spam_inv)
