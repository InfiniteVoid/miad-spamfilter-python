# MIAD-SpamFilter-Python

База тестових листів для аналізу взята звідси: https://spamassassin.apache.org/old/publiccorpus/

Тут використаний наївний баєсовський алгоритм для оцінки листів: https://en.wikipedia.org/wiki/Naive_Bayes_spam_filtering

Запустити програму можна командою: `python app.py spam_examples ham_examples spam_test ham_test`

З трохи зміненими аргументами: `python app.py spam_examples ham_examples spam_test ham_test -init_prob_spam 0.7 -occurence_threshold 5`

Програма може оцінювати відразу багато тестових імейлів з папок `spam_examples` та `ham_examples`. Або можна створити нову тестову папку з одним імейлом і вказати в параметрах.
