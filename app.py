from argparse import ArgumentParser
from spam_filter_bayes import *

parser = ArgumentParser(description="Spam Filter A. Romanyshyn")
parser.add_argument("spam_examples", help="spam example messages folder")
parser.add_argument("ham_examples", help="ham example messages folder")
parser.add_argument("spam_test", help="test messages folder")
parser.add_argument("ham_test", help="test messages folder")
# Початкова вірогідність, що повідомлення це спам
parser.add_argument("-init_prob_spam", help="initial probability of a message being spam (0-1)", default="0.5", type=float)
# Кількість разів повторення слова для того, щоб воно потрапило в процес розрахунків. Інакше слово буде проігноровано
parser.add_argument("-occurence_threshold", help="number of times a word must appear in spam and ham messages overall", default="10", type=int)
# Бал вище якого потрібно оцінити повідомлення, щоб воно потрапило до спаму
parser.add_argument("-score_threshold", help="spam score above which a message must be rated to be considered spam (0-1)", default="0.9", type=float)
args = parser.parse_args()

# Стала вірогідність що слист це спам (0.5 за замовчуванням)
init_prob_spam = args.init_prob_spam

# Отримати межі
occurence_threshold = args.occurence_threshold
score_threshold = args.score_threshold

# Зчитуємо дані з тестових файлів 
spam_example_messages = get_messages(args.spam_examples)
ham_example_messages = get_messages(args.ham_examples)
spam_test_messages = get_messages(args.spam_test)
ham_test_messages = get_messages(args.ham_test)

# Отримати список слів з повідомлень
spam_words = get_messages_words(spam_example_messages)
ham_words = get_messages_words(ham_example_messages)
spam_test_words = get_messages_words(spam_test_messages)
ham_test_words = get_messages_words(ham_test_messages)

# Отримати загальну частоту появи слів в спам і не спам листах
spam_word_frequencies = get_word_frequencies(spam_words)
ham_word_frequencies = get_word_frequencies(ham_words)

# Отримати коефіцієнт спамності для кожного слова
word_spamicities = get_word_spamicities(spam_word_frequencies, ham_word_frequencies, init_prob_spam, occurence_threshold)

# Кількість підтверджених на спам чи не спам повідомлень
spam_successes = 0
ham_successes = 0

# Перевіряємо тестові є-мейли на спам
for key in sorted(spam_test_words):
    message_score = get_spam_score(spam_test_words[key], word_spamicities)
    
    if message_score > score_threshold:
        spam_successes += 1

for key in sorted(ham_test_words):
    message_score = get_spam_score(ham_test_words[key], word_spamicities)
    
    if message_score < score_threshold:
        ham_successes += 1

spam_success_rate = spam_successes / len(spam_test_messages)
ham_success_rate = ham_successes / len(ham_test_messages)

# Вивести відсоток спам повідомленя у вказаній тестовій папці для спам повідомлень
print("Percent of spam mails in test spam folder: {0:.2f}%".format(spam_success_rate * 100))
# Вивести відсоток НЕ спам повідомленя у вказаній тестовій папці для НЕ спам повідомлень
print("Percent of ham mails in test ham folder: {0:.2f}%".format(ham_success_rate * 100))
